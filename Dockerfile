FROM jupyter/scipy-notebook

USER root

WORKDIR /usr/src
RUN mkdir workspace

COPY notebooks ./workspace/notebooks
COPY requirements-conda.txt .
COPY requirements-pip.txt .

RUN conda install --file requirements-conda.txt
RUN pip install -r requirements-pip.txt

CMD ["jupyter", "notebook", "--notebook-dir=workspace", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]
