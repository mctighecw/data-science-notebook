# README

A basic [Jupyter Notebook](https://jupyter.org) for data science using Python, with commonly used packages such as:

- NumPy
- Pandas
- SciPy
- Matplotlib
- Seaborn
- Plotly
- Cufflinks
- Scikit-learn
- Beautiful Soup
- Natural Language Toolkit (NLTK)

Other packages can be added in the `conda` and `pip` requirements files.

_Note_:
Notebooks saved locally while Docker running will not be persisted. They need to be downloaded to computer desktop and then manually copied to the `notebooks/` directory, to be available later.

## App Information

App Name: data-science-notebook

Created: February 2021

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/data-science-notebook)

## Tech Stack

- Python 3.8.6 (current version included in _Jupyter Notebook_ image)
- [SciPy Notebook Image](https://hub.docker.com/r/jupyter/scipy-notebook)
- Various Python data science packages (see above) installed
- Docker

## To Run

1. Build network with `docker-compose` and show live console log

```
$ docker-compose up --build
```

2. Check console for Notebook token and copy into browser

```
> http://127.0.0.1:8888/?token=NOTEBOOK_TOKEN
```

3. Go to `localhost:8888`, enter token, and start Notebook

Last updated: 2025-03-08
